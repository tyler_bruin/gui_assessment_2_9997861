﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace gui_assessment_2_9997861
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
        public sealed partial class MainPage : Page
        {
            public string printTxt = "";
            public string printFrom = "";
            public string printMessage = "";
            public int printPriority = 0;

            public int optPriority = 0;
            public int optDepartment = 0;

            public MainPage()
            {
                this.InitializeComponent();

                var department = new List<string>();
                department.Add("Production ");
                department.Add("Research and Development");
                department.Add("Purchasing");
                department.Add("Marketing");
                department.Add("Human Resource Management");
                department.Add("Accounting and Finance");

                comboBoxTo.ItemsSource = department;

                var priority = new List<string>();
                priority.Add("No Priority");
                priority.Add("Low Priority");
                priority.Add("Medium Priority");
                priority.Add("High Priority");
                priority.Add("Urgent Priority");


                comboBoxSubject.ItemsSource = priority;


            }


            private async void buttonSubmit_Click(object sender, RoutedEventArgs e)
            {


                printTxt = textBoxMessage.Text;
                printFrom = textBoxFrom.Text;
                optPriority = comboBoxSubject.SelectedIndex;
                optDepartment = comboBoxTo.SelectedIndex;
                printMessage = $"Are you sure want to send this message?\n\nTo: {optDepartmentprint(optDepartment)}\nFrom: {printFrom}\nSubject: {optPriorityprint(optPriority)}\n\nMessage:\n{printTxt}";

                var dialog = new MessageDialog($"{printMessage}");
                dialog.Title = "Confirm Message";
                dialog.Commands.Add(new UICommand { Label = "Send", Id = 0 });
                dialog.Commands.Add(new UICommand { Label = "Reset", Id = 1 });
                dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 2 });
                var res = await dialog.ShowAsync();

                if ((int)res.Id == 0)
                {
                    Application.Current.Exit();
                }
                else if ((int)res.Id == 1)
                {
                    comboBoxTo.SelectedIndex = -1;
                    textBoxFrom.Text = "";
                    comboBoxSubject.SelectedIndex = -1;
                    textBoxMessage.Text = "";

                }

            }

            static string optPriorityprint(int optPriority)
            {
                var optP = "";

                switch (optPriority)
                {
                    case 0:
                        optP = "No Priority";
                        break;
                    case 1:
                        optP = "Low Priority";
                        break;

                    case 2:
                        optP = "Medium Priority";
                        break;

                    case 3:
                        optP = "High Priority";
                        break;

                    case 4:
                        optP = "Urgent Priority";
                        break;

                }
                return optP;
            }
            static string optDepartmentprint(int optDepartment)
            {
                var optD = "";

                switch (optDepartment)
                {
                    case 0:
                        optD = "Production";
                        break;
                    case 1:
                        optD = "Research and Development";
                        break;

                    case 2:
                        optD = "Purchasing";
                        break;

                    case 3:
                        optD = "Marketing";
                        break;

                    case 4:
                        optD = "Human Resource Management";
                        break;

                    case 5:
                        optD = "Accounting and Finance";
                        break;

                }
                return optD;
            }
        }
    }